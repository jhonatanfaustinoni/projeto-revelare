<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Banner;
use App\Models\Pergunta;

class FrontController extends Controller{

    public function index()
    {
        
        $banners = Banner::orderBy('id', 'ASC')->paginate(5);
        
        return view('index',compact('banners'));
           
    }

    public function faq(){

        $perguntas = Pergunta::orderBy('id', 'DESC')->paginate(5);
    
        return view('faq',compact('perguntas'));
    }

   
}