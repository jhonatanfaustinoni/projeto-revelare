<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Pergunta
 * @package App\Models
 * @version October 28, 2018, 4:40 pm UTC
 *
 * @property string titulo
 * @property string resposta
 */
class Pergunta extends Model
{
    use SoftDeletes;

    public $table = 'perguntas';
    

    protected $dates = ['deleted_at'];


    public $fillable = [
        'titulo',
        'resposta'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'titulo' => 'string',
        'resposta' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'titulo' => 'required',
        'resposta' => 'required'
    ];

    
}
