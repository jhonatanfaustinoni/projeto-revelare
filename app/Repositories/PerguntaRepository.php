<?php

namespace App\Repositories;

use App\Models\Pergunta;
use InfyOm\Generator\Common\BaseRepository;

/**
 * Class PerguntaRepository
 * @package App\Repositories
 * @version October 28, 2018, 4:40 pm UTC
 *
 * @method Pergunta findWithoutFail($id, $columns = ['*'])
 * @method Pergunta find($id, $columns = ['*'])
 * @method Pergunta first($columns = ['*'])
*/
class PerguntaRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'titulo',
        'resposta'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Pergunta::class;
    }
}
