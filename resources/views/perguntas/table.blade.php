<table class="table table-responsive" id="perguntas-table">
    <thead>
        <tr>
            <th>Titulo</th>
        <th>Resposta</th>
            <th colspan="3">Ações</th>
        </tr>
    </thead>
    <tbody>
    @foreach($perguntas as $pergunta)
        <tr>
            <td>{!! $pergunta->titulo !!}</td>
            <td>{!! $pergunta->resposta !!}</td>
            <td>
                {!! Form::open(['route' => ['perguntas.destroy', $pergunta->id], 'method' => 'delete']) !!}
                <div class='btn-group'>
                    <a href="{!! route('perguntas.show', [$pergunta->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-zoom-in"></i></a>
                    <a href="{!! route('perguntas.edit', [$pergunta->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-edit"></i></a>
                    {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Deseja realmente excluir a pergunta?')"]) !!}
                </div>
                {!! Form::close() !!}
            </td>
        </tr>
    @endforeach
    </tbody>
</table>
{{ $perguntas->links() }}