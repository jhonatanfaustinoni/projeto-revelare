<!-- Titulo Field -->
<div class="form-group col-sm-6">
    {!! Form::label('titulo', 'Titulo:') !!}
    {!! Form::text('titulo', null, ['class' => 'form-control']) !!}
</div>

<!-- Resposta Field -->
<div class="form-group col-sm-12 col-lg-12">
    {!! Form::label('resposta', 'Resposta:') !!}
    {!! Form::textarea('resposta', null, ['class' => 'form-control', 'id' => 'editor']) !!}
</div>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Salvar', ['class' => 'btn btn-primary']) !!}
    <a href="{!! route('perguntas.index') !!}" class="btn btn-default">Cancelar</a>
</div>
