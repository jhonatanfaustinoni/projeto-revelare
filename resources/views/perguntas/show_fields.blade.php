<!-- Id Field -->
<div class="form-group">
    {!! Form::label('id', 'Id:') !!}
    <p>{!! $pergunta->id !!}</p>
</div>

<!-- Titulo Field -->
<div class="form-group">
    {!! Form::label('titulo', 'Titulo:') !!}
    <p>{!! $pergunta->titulo !!}</p>
</div>

<!-- Resposta Field -->
<div class="form-group">
    {!! Form::label('resposta', 'Resposta:') !!}
    <p>{!! $pergunta->resposta !!}</p>
</div>

<!-- Created At Field -->
<div class="form-group">
    {!! Form::label('created_at', 'Criado em:') !!}
   
    <p> {!!$pergunta->created_at->format('d/m/Y')!!}</p>
</div>

<!-- Updated At Field -->
<div class="form-group">
    {!! Form::label('updated_at', 'Alterado em:') !!}
    <p> {!!$pergunta->updated_at->format('d/m/Y')!!}</p>
</div>

