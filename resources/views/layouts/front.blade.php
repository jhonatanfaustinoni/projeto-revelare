<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
    <link rel="stylesheet" href="{{ asset('css/style.scss') }}">
    <link rel="stylesheet" href="{{ asset('css/style.css') }}">

    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.3.1/css/all.css" integrity="sha384-mzrmE5qonljUremFsqc01SB46JvROS7bZs3IO2EmfFsd15uHvIt+Y8vEf7N7fWAU" crossorigin="anonymous">

    <title>Desafio Revelare</title>
  </head>
  <body>
    <nav class="navbar navbar-expand-lg navbar-light  fixed-top">
      <div class="container">
        <div class="col-3">
          <a class="navbar-brand" href="{{ url('/') }}"><img src="{{ asset('images/logo.png') }}" class="img-fluid"></a>
        </div>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
          <span class="navbar-toggler-icon"></span>
        </button>

        <div class="col-9">
          <div class="collapse navbar-collapse" id="navbarSupportedContent">
            <ul class="navbar-nav ml-auto">
              <li class="nav-item">
                <a class="nav-link" href="{{ url('/') }}">Home</a>
              </li>
              <li class="nav-item">
                <a class="nav-link" href="{{ url('/') }}">Planos</a>
              </li>
              <li class="nav-item">
                <a class="nav-link" href="{{ url('/faq') }}">Faq</a>
              </li>
              <li class="nav-item">
                <a class="nav-link" href="{{ url('/') }}">Contato</a>
              </li>
              <li class="nav-item">
                <a class="nav-link" target="_blank" href="{{ url('https://www.facebook.com/AgenciaRevelare/?ref=br_rs') }}"><i class="fab fa-facebook"></i></a>
              </li>
            </ul>
          </div>
        </div>
      </div>
    </nav>




   
  
   <!--  end header -->

   <!-- content -->

   @yield('content')


   <!-- end content -->

   <!-- footer -->

     <div class="assinatura">
    <div class="container">
      <div class="logo-roda">
        <span>© 2018 major axis, todos os direitos reservados. Powered by</span>
        <img class="img-fluid" src="{{ asset('images/logo-rodape.png') }}"></div>
    </div>
  </div>
 

   <!-- end footer -->
   

    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
  <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>
  <script>
var acc = document.getElementsByClassName("accordion");
var i;

for (i = 0; i < acc.length; i++) {
  acc[i].addEventListener("click", function() {
    this.classList.toggle("active");
    var panel = this.nextElementSibling;
    if (panel.style.maxHeight){
      panel.style.maxHeight = null;
    } else {
      panel.style.maxHeight = panel.scrollHeight + "px";
    } 
  });
}
</script>

<script>
		$(function(){   
			var nav = $('.navbar');   
			$(window).scroll(function () { 
				if ($(this).scrollTop() > 150) { 
					nav.addClass("banner--stick"); 
				} else { 
					nav.removeClass("banner--stick"); 
				} 
				$("a.anchor").removeClass('content-anchor');

				if (window.location.hash && $("a.anchor").length) {
					if (window.location.hash.replace('#', '') == $("a.anchor").attr('name') ) {
						if ($(this).scrollTop() >= $('.anchor').offset().top) {
							$("a.anchor").addClass('content-anchor');	
						}  else {
							$("a.anchor").removeClass('content-anchor');	
						}
						
					}
				} 
			});  
			
		});

	</script>
  </body>
</html>