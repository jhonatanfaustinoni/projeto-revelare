@extends('layouts.front')

@section('content')

  <div class="hero" style="background:url('{{ asset('images/banner.png') }}')no-repeat center center;">
    <div class="container">
        <h1>
          Quer organizar as atividades<br>
          de sua empresa de um modo<br>
          econômico e eficiente?
        </h1>
        <a class="btn btn-experimente" href="#">Experimente agora!</a>
    </div>
    
    <img src="{{ asset('images/mouse.png') }}" class="img-fluid mouse">
  </div>

  <div class="sobre">
    <div class="container">
      <div class="row">
        <div class="col-md-6 col-xs-12 text-sobre">
          <h2>O que é o major axis?</h2>

          <p>O <strong>Major Axis</strong> é um sistema <strong>PCP (planejamento e controle da produção)</strong> que permite gerenciar, prever e otimizar o uso dos recursos de sua empresa em qualquer processo, produtivo ou não, que envolva a sistematização em etapas, acarretando em maior controle e melhores resultados.</p>
        </div>
        <div class="col-md-6 col-xs-12">
          <img src="{{ asset('images/oque.png') }}" class="img-fluid">
        </div>
      </div>
      <div class="row">
        <a class="btn-pcp" href="">pcp = planejamento e controle de produção</a>
      </div>
    </div>
  </div>

  <div class="funcionamento" style="background:url('{{ asset('images/bgfuncionamento.png') }}')no-repeat center center;">
    <div class="container">
        <div class="row">
          <div class="col">
            <h2>Funcionamento</h2>

            <p class="sub">Com o PCP, é possível trabalhar objetivos e resultados para a sua empresa a curto, médio e longo prazo:</p>
          </div>
        </div>
        <div class="row">
          <div class="col-md-4 col-xs-12">
            <div class="box-func">
              <div class="title">
                <h4>Nível operacional</h4>
              </div>
              <div class="content">
                <p class="box-1">Definição de prazos e controle do que já foi planejado</p>
                <div class="prazo"><img src="{{ asset('images/curto.png') }}" class="img-fluid"> <span>Curto Prazo</span></div>
              </div>
            </div>
          </div>

          <div class="col-md-4 col-xs-12">
            <div class="box-func">
              <div class="title">
                <h4>Nível tático</h4>
              </div>
              <div class="content">
                <p class="box-1">Criação do plano mestre de produção</p>
                <div class="prazo"><img src="{{ asset('images/medio.png') }}" class="img-fluid"> <span>Médio Prazo</span></div>
              </div>
            </div>
          </div>

          <div class="col-md-4 col-xs-12">
            <div class="box-func">
              <div class="title">
                <h4>Nível estratégico</h4>
              </div>
              <div class="content">
                <p>Definição de metas e objetivos do negócio a partir da análise de capacidade da empresa</p>
                <div class="prazo"><img src="{{ asset('images/longo.png') }}" class="img-fluid"> <span>Longo Prazo</span></div>
              </div>
            </div>
          </div>
        </div>

        <div class="row">
          <div id="banners" class="carousel slide" data-ride="carousel">
            <div class="col-md-12">
              <div class="carousel-inner">
                @php
                  $active = 2;
                @endphp

                @foreach($banners as $banner)
                  @if( $active == 2 )
                    <div class="carousel-item active">
                      <img src="{{ $banner->image}}">
                    </div>
                    @php
                      $active = 1;
                    @endphp
                    @else
                      <div class="carousel-item ">
                        <img src="{{ $banner->image}}">
                      </div>
                  @endif
                @endforeach
    
              </div>

              <a class="carousel-control-prev" href="#banners" data-slide="prev">
                <span class="carousel-control-prev-icon"></span>
              </a>
              <a class="carousel-control-next" href="#banners" data-slide="next">
                <span class="carousel-control-next-icon"></span>
              </a>
            </div>
          </div>
        </div>
    </div>
  </div>

  <div class="vantagens">
    <div class="container">
      <h2>Vantagens</h2>
      <p class="resumo-van">Conheça algumas vantagens de usar o Major Axis:</p>
    </div>
    <div class="row">
      <div class="col-md-4 col-id-1">
        <div class="numero id-1"><span></span>1</div><h4 class="title-van">Aumento de eficiência:</h4>
        <p class="text-van">a programação do sistema permite reduzir desperdícios, utilizar melhor o tempo e otimizar a produtividade como um todo.</p>
      </div>

       <div class="col-md-4 col-id-2">
        <div class="numero id-2"><span></span>2</div><h4 class="title-van">Redução de custos:</h4>
        <p class="text-van">com a melhoria do processo produtivo e aumento da eficiência, é possível chegar a uma redução de custos significativa, tornando o negócio mais competitivo.</p>
      </div>

       <div class="col-md-4 col-id-3">
        <div class="numero id-3"><span></span>3</div><h4 class="title-van">Integração de processos e setores:</h4>
        <p class="text-van">a programação do sistema permite reduzir desperdícios, utilizar melhor o tempo e otimizar a produtividade como um todo.</p>
      </div>
    </div>

    <div class="row">
      <div class="col-md-4 col-id-4">
        <div class="numero id-4"><span>4</span></div><h4 class="title-van">Melhoria da comunicação:</h4>
        <p class="text-van">como diversas pessoas e setores estão envolvidos no processo produtivo, é necessário que haja troca de informações, culminando em um clima organizacional mais favorável.</p>
      </div>

      <div class="col-md-4 col-id-5">
        <div class="numero id-5"><span></span>5</div><h4 class="title-van">Sistematização de processos:</h4>
        <p class="text-van">utilizando o planejamento e programação do sistema, cada setor/colaborador sabe o que deve fazer, quando, como e em
que ritmo fazer.</p>
      </div>

      <div class="col-md-4 col-id-6">
        <div class="numero id-6"><span></span>6</div><h4 class="title-van">Aumento da segurança:</h4>
        <p class="text-van">por meio das ferramentas e etapas do Major Axis, é possível realizar previsão de demanda, controlar sazonalidade e produzir
somente o necessário.</p>
      </div>
    </div>

    <div class="row">
      <div class="col-md-4 col-id-7">
        <div class="numero id-7"><span>7</span></div><h4 class="title-van">Mensuração da ociosidade:</h4>
        <p class="text-van">tenha informações precisas sobre taxas de ociosidade de máquinas, equipamentos e mesmo mão de obra.</p>
      </div>

      <div class="col-md-4 col-id-8">
        <div class="numero id-8"><span></span>8</div><h4 class="title-van">Análise de dados para a tomada
de decisão:</h4>
        <p class="text-van">com os relatórios gerados pelo sistema, é possível embasar com grande margem de confiança diversos processos decisórios.</p>
      </div>

      <div class="col-md-4 col-id-9">
        <div class="numero id-9"><span></span>9</div><h4 class="title-van">Personalização:</h4>
        <p class="text-van">todas as funções presentes no Major Axis podem ser personalizadas conforme as necessidades e especificidades da empresa.</p>
      </div>
    </div>
  </div>

  <div class="aplic">
    <div class="container">
      <h2>Aplicabilidade</h2>
      <p class="resumo-van">Conheça algumas vantagens de usar o Major Axis:</p>

      <div class="row">
        <div class="col-md-3 col-sm-6 col-xs-12">
          <img src="{{ asset('images/agencia.png') }}" class="img-fluid">
          <h4>Agências de Publicidade</h4>
        </div>

        <div class="col-md-3 col-sm-6 col-xs-12">
          <img src="{{ asset('images/tec.png') }}" class="img-fluid">
          <h4>Agências de Tecnologia</h4>
        </div>

        <div class="col-md-3 col-sm-6 col-xs-12">
          <img src="{{ asset('images/design.png') }}" class="img-fluid">
          <h4>Agências de Design</h4>
        </div>

        <div class="col-md-3 col-sm-6 col-xs-12">
          <img src="{{ asset('images/graficas.png') }}" class="img-fluid">
          <h4>Gráficas</h4>
        </div>

      </div>
    </div>
  </div>

  <div class="contato" style="background:url('{{ asset('images/bgcontato.png') }}')no-repeat center center;">
    <div class="container">
      <h2>Cadastro</h2>
      <p class="resumo-van">Realize agora mesmo seu <strong>cadastro GRATUITO</strong></p>

      <div class="form-rodape">
        <form>
          <div class="form-group">
            <input type="text" class="form-control" id="empresa" placeholder="Nome da empresa*">
          </div>
          <div class="form-group">
            <input type="text" class="form-control" id="nome" placeholder="Nome*">
          </div>
          <div class="form-group">
            <input type="email" class="form-control" id="email" placeholder="E-mail*">
          </div>

          <div class="form-group">
            <input type="text" class="form-control" id="telefone" placeholder="Telefone*">
          </div>

          <div class="form-group">
            <input type="text" class="form-control" id="usuario" placeholder="Usuário*">
          </div>
          
          <div class="form-group">
            <input type="password" class="form-control" id="senha" placeholder="Senha*">
          </div>
          
          <div class="form-group form-check">
            <input type="checkbox" class="form-check-input" id="novidades">
            <label class="form-check-label">Quero receber conteúdo com novidades da Ferramenta.</label>
          </div>
          <button type="submit" class="btn btn-primary">Cadastrar Gratuitamente</button>
        </form>
      </div>
    </div>
  </div>






  


@endsection()

